package com.example.labhyasharma.taskupwork

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import android.widget.TextView

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.TimeZone

class GetUTCTime : AppCompatActivity() {


    private val OUTPUT_DATE_FORMATE = "dd-MM-yyyy - hh:mm a"

    internal lateinit var textView : TextView


    val utcTime: String
        get() {

            LongOperation().execute("")

            return ""
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_utctime)

        //        Log.d("UTC", "onCreate: "+ getUTCTime());

        textView = findViewById(R.id.dateTime)

        findViewById<View>(R.id.fetchButton).setOnClickListener {
            utcTime
            textView.text = "Fetching..."
        }
    }


    private inner class LongOperation : AsyncTask<String, Void, Long>() {

        override fun doInBackground(vararg params: String): Long? {
            var nowAsPerDeviceTimeZone: Long = 0
            try {
                val sntpClient = SntpClient()

                if (sntpClient.requestTime("ntp.ubuntu.com", 30000)) {
                    nowAsPerDeviceTimeZone = sntpClient.ntpTime


                }
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                Thread.interrupted()
            }

            return nowAsPerDeviceTimeZone
        }

        override fun onPostExecute(nowAsPerDeviceTimeZone: Long?) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you

            Log.d("GMT", "getUTCTime:0 " + Date(nowAsPerDeviceTimeZone!!))
            textView.text = Date(nowAsPerDeviceTimeZone).toString()

            //            Calendar cal = Calendar.getInstance();
            //            TimeZone timeZoneInDevice = cal.getTimeZone();
            //            int differentialOfTimeZones = timeZoneInDevice.getOffset(System.currentTimeMillis());
            //            nowAsPerDeviceTimeZone -= differentialOfTimeZones;
            //
            //
            //
            //
            //
            //            Log.d("GMT", "getUTCTime:1 "+ new Date(nowAsPerDeviceTimeZone).toString());
        }

        override fun onPreExecute() {}

        override fun onProgressUpdate(vararg values: Void) {}
    }


}
